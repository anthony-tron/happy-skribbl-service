const { io } = require('socket.io-client');
const assert = require('assert');

const origin = 'https://skribbl.io:4999';

const params = {
  avatar: [9, 10, 23, -1],
  code: '',
  createPrivate: true,
  join: '',
  language: 'English',
  name: 'Animator',
};

const words = ['thousand', 'cherry', 'tree'];

describe('client', function () {
  let socket;

  before((done) => {
    const { HOSTNAME, PORT } = process.env;

    socket = io(`http://${HOSTNAME}:${PORT}`);
    socket.on('connect', done);
  });

  after((done) => {
    socket.close();
    done();
  });

  it('should receive "gameCreated" after emitting "createGame"', (done) => {
    socket.on('gameCreated', ({ key }) => {
      assert.equal(typeof key, 'string');
      done();
    });

    socket.emit('createGame', { origin, params });
  });

  it('should receive "gameStarted" after emitting "startGame"', (done) => {
    socket.on('gameStarted', done);
    socket.emit('startGame', { words });
  });
});
