const express = require('express');
const http = require('http');
const { Server } = require('socket.io');
const { createGame } = require('happy-skribbl');
const assert = require('assert');

const { HOSTNAME, PORT } = process.env;

try {
  assert.equal(typeof HOSTNAME, 'string');
  assert.match(PORT, /^\d+$/);
} catch (err) {
  console.error('Missing HOSTNAME and PORT environment variables.');
  process.exit(1);
}

const app = express();
const server = http.createServer(app);
const io = new Server(server, {
  cors: {
    origin: '*',
  },
});

io.on('connection', (socket) => {
  socket.on('createGame', async ({ origin, params }) => {
    const { start, key } = await createGame(origin, params);

    socket.startGame = start;
    socket.emit('gameCreated', { key });
  });

  socket.on('startGame', async ({ words }) => {
    await socket.startGame(words);

    socket.emit('gameStarted');

    delete socket.startGame;
  });
});

server.listen(PORT, HOSTNAME, () => {
  console.log(`Listening on ${HOSTNAME}:${PORT}`);
});
